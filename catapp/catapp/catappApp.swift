//
//  catappApp.swift
//  catapp
//
//  Created by eddier caicedo on 23/08/21.
//

import SwiftUI

@main
struct catappApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
