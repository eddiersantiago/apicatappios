//
//  ContentView.swift
//  catapp
//
//  Created by eddier caicedo on 23/08/21.
//

import SwiftUI
struct ContentView: View {
    @ObservedObject var imageLoader: ImageLoaderMV = ImageLoaderMV()
    @ObservedObject var breedViewModel : BreedViewModel = BreedViewModel()
    @State var selection = 0
    init() {
        breedViewModel.setImageLoaderMV(_imageLoader: imageLoader)
        breedViewModel.getBreed()
    }
    var body: some View {
            TabView(selection: $selection){
                VoteBreedView(imageLoader: imageLoader, breedViewModel: breedViewModel).tabItem{
                    Image(systemName: "heart.fill")
                }.tag(0)
              
                ListInformationBreedView().environmentObject(breedViewModel).tabItem{
                    Image(systemName: "heart.text.square")
                }.tag(1)
            }
        }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



