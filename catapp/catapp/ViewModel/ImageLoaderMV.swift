//
//  ImageLoaderMV.swift
//  catapp
//
//  Created by eddier caicedo on 26/08/21.
//

import SwiftUI
import Combine
class ImageLoaderMV: ObservableObject {
    var didChange = PassthroughSubject <Data,Never > ()
    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }
    

    
  func loadImage(urlString:String) {
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }
        task.resume()
    }
}
