//
//  BreedViewModel.swift
//  catapp
//
//  Created by eddier caicedo on 26/08/21.
//

import SwiftUI
import Combine
class BreedViewModel : ObservableObject {
    @Published var breeds : [BreedModel]?  = [BreedModel]()
    @Published var breedLikes : [BreedLikeModel]? = [BreedLikeModel] ()
    @Published var breedImage: [BreedImageModel] = [BreedImageModel]()
    let APIKEY : String  = "d314ad9a-d1a0-4ff5-aabf-d8e06e4fc4d5"
    let URLAPI : String = "https://api.thecatapi.com/v1/breeds"
    
    @ObservedObject var imageLoader: ImageLoaderMV = ImageLoaderMV()
    init() {
        guard let data =  UserDefaults.standard.data(forKey: "breedLikes"),
              let saveItem = try? JSONDecoder().decode([BreedLikeModel].self, from: data)
        else{return}
        breedLikes = saveItem
    }
    
    func setImageLoaderMV(_imageLoader: ImageLoaderMV){
        self.imageLoader = _imageLoader
    }
    
    func getBreed() {
        breeds = [BreedModel]()
        breeds = [BreedModel(id: "", name: "", description: "", origin: "")]
        guard let url = URL(string: URLAPI) else {return}
        URLSession.shared.dataTask(with: url) { (data, res, err) in
            do {
                if let data = data{
                    let decoder = JSONDecoder()
                    let result = try decoder.decode([BreedModel].self, from: data)
                    DispatchQueue.main.async {
                        self.breeds = result
                        self.getImageBreed(idBreed: self.breeds![0].id )
                       // self.loadImage(reference: self.breeds![0].reference_image_id )
                      //  self.breeds?.insert(BreedModel(id: "", name: "Seleccionar Raza", description: "", origin: "", image: ImageModel(url: "")), at: 0)
                    }
                }else{
                    print("no hay datos")
                }
            } catch (let error ){
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func getImageBreed(idBreed: String){
        let url_api = "https://api.thecatapi.com/v1/images/search?breed_ids=\(idBreed)&limit=1"
        guard let url = URL(string: url_api) else {return}
        URLSession.shared.dataTask(with: url) { (data, res, err) in
            do {
                if let data = data{

                    print(data)
                    let decoder = JSONDecoder()
                    let result = try decoder.decode([BreedImageModel].self, from: data)
                    DispatchQueue.main.async {
                        self.breedImage = result
                        self.loadImage(urlImage: self.breedImage[0].url)
                    }
                }else{
                    print("no hay datos")
                }
            } catch (let error ){
                print(error.localizedDescription)
            }
        }.resume()
    }
    func loadImage(urlImage : String)  {
        DispatchQueue.main.async {
            self.imageLoader.loadImage(urlString: urlImage)
        }
    }
    
    
    
    func saveLikeBreed(like : Bool, indexBreed : Int, image : UIImage){
        let date = getDate()
        let imageData : Data = image.pngData()!
        let breedsave = BreedLikeModel(id: UUID(), fecha: date, like: like, image: imageData, name: self.breeds![indexBreed].name, description: self.breeds![indexBreed].description,origin: self.breeds![indexBreed].origin )
        breedLikes?.append(breedsave)
        if let encodeData = try? JSONEncoder().encode(breedLikes){
            UserDefaults.standard.set(encodeData, forKey: "breedLikes")
        }
        self.breeds?.remove(at: indexBreed)
        self.getImageBreed(idBreed: self.breeds![indexBreed].id)
    }
    
    private func getDate() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter.string(from: now)
    }
}
