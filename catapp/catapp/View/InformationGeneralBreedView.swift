//
//  InformationCatView.swift
//  catapp
//
//  Created by eddier caicedo on 28/08/21.
//

import SwiftUI

struct InformationGeneralBreedView: View {
    @State var breedLikes : BreedLikeModel
    var body: some View {
        ScrollView{
        
               
                VStack{
                    Image(uiImage: UIImage(data: breedLikes.image)!).resizable()
                        .scaledToFit()
                        .frame(width: UIScreen.main.bounds.width)
                    ZStack{
                        RoundedRectangle(cornerRadius: 0.0, style: .continuous).fill(Color.white)
                                               .frame(width: UIScreen.main.bounds.width, height: 60, alignment: .center)
                                               .shadow( color: Color.black.opacity(0.05) ,radius: 10, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 5.0)
                        Text("Raza: \(breedLikes.name)").frame(width: UIScreen.main.bounds.width-20, alignment: .leading).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        Text("Origen: \(breedLikes.origin)").frame(width: UIScreen.main.bounds.width-20, alignment: .trailing).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    }
                    ZStack{
                        RoundedRectangle(cornerRadius: 0.0, style: .continuous).fill(Color.white)
                                               .frame(width: UIScreen.main.bounds.width, height: 60, alignment: .center)
                                               .shadow( color: Color.black.opacity(0.05) ,radius: 10, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 5.0)
                        Text("Fecha Votacion: \(breedLikes.fecha)").frame(width: UIScreen.main.bounds.width-20, alignment: .leading).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    }
                    ZStack{
                        RoundedRectangle(cornerRadius: 0.0, style: .continuous).fill(Color.white)
                                               .frame(width: UIScreen.main.bounds.width, height: 60, alignment: .center)
                                               .shadow( color: Color.black.opacity(0.05) ,radius: 10, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 5.0)
                       
                        Text("Votacion: \(breedLikes.like == true ? "Me gusta" : "No me gusta")").frame(width: UIScreen.main.bounds.width-20, alignment: .leading).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    }
                    
                    ZStack{
                        RoundedRectangle(cornerRadius: 0.0, style: .continuous).fill(Color.white)
                                               .frame(width: UIScreen.main.bounds.width, height: 200, alignment: .center)
                                               .shadow( color: Color.black.opacity(0.05) ,radius: 10, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 5.0)
                       
                        Text("Descripción: \(breedLikes.description)").frame(width: UIScreen.main.bounds.width-20, alignment: .leading).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    }
                }.frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: .top)
                
            }
        
        
       
    }
}

struct InformationCatView_Previews: PreviewProvider {
    static var previews: some View {
        InformationGeneralBreedView(breedLikes: BreedLikeModel(id: UUID(), fecha: "", like: false, image: Data(), name: "", description: "", origin: ""))
    }
}
