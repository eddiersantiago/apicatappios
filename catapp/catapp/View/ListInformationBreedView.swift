//
//  InformationView.swift
//  catapp
//
//  Created by eddier caicedo on 28/08/21.
//

import SwiftUI

struct ListInformationBreedView: View {
    //@ObservedObject var breedViewModel: BreedViewModel = BreedViewModel()
    @EnvironmentObject var breedViewModel : BreedViewModel
    var body: some View {
        NavigationView{
            List(breedViewModel.breedLikes!, id: \.self){ breed in
                
                NavigationLink(
                    destination: InformationGeneralBreedView(breedLikes: breed),
                    label: {
                        Image(uiImage: UIImage(data: breed.image)!).resizable()
                            .scaledToFit()
                            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
                        VStack{
                            Text("Nombre: \(breed.name)")
                            
                            if breed.like == true{
                                Text("Votacion: Me gusta")
                            }else{
                                Text("Votacion: No me gusta")
                            }
                        }
                      
                                
                          
                             

                    })
            }.navigationTitle("Lista De Razas")
        }
        
          
        
    }
}

struct InformationView_Previews: PreviewProvider {
    static var previews: some View {
        ListInformationBreedView( )
    }
}
