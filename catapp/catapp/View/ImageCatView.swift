//
//  ImageCatView.swift
//  catapp
//
//  Created by eddier caicedo on 28/08/21.
//

import SwiftUI

struct VoteBreedView: View {
    @State var indexSeleted : Int = 0
    
    @State var indexSelectedCat : Int = 0
    @State var image:UIImage = UIImage()
    @State private var selection = 0
    @ObservedObject var imageLoader: ImageLoaderMV
    @ObservedObject var breedViewModel : BreedViewModel
    @State var loadingImage : Bool = true
    @State var isDesactiveButtonLike : Bool = true
//    init() {
//        self.breedViewModel.setImageLoaderMV(_imageLoader: imageLoader)
//        breedViewModel.getBreed()
//    }
    var body: some View {
        NavigationView{
            VStack{
                VStack{
                    HStack{
                        Text("Seleccionar raza")
                            Picker(selection: $indexSeleted, label:
                                    HStack{
                                        Text("\(breedViewModel.breeds![indexSeleted].name)")
                                        Image(systemName: "chevron.down")
                                    }.frame(width: 250)
                                    , content: {
                                        ForEach(breedViewModel.breeds!.indices, id:\.self) {index in
                                            Text(breedViewModel.breeds![index].name).tag(index)
                                        
                                }
                                    }).onChange(of: indexSeleted, perform: { value in
                                        print(value)
                                                loadingImage.toggle()
                                                isDesactiveButtonLike.toggle()
                                                self.image = UIImage()
                                                breedViewModel.getImageBreed(idBreed: breedViewModel.breeds![value].id)
                                    }).pickerStyle(MenuPickerStyle())
                        
                        }
                    }
                ZStack{
                    if(loadingImage){
                        Text("Cargando Imagen...")
                    }
                    Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width:UIScreen.main.bounds.width)
                    .onReceive(imageLoader.didChange) { data in
                    self.image = UIImage(data: data) ?? UIImage()
                        loadingImage.toggle()
                        isDesactiveButtonLike.toggle()
                        print("ya descargo")
                    }
                   
                }.frame(height:UIScreen.main.bounds.height * 0.3)
                    HStack{
                        Button(action: {
                            isDesactiveButtonLike.toggle()
                            breedViewModel.saveLikeBreed(like: false, indexBreed: indexSeleted, image: self.image)
                        }, label: {
                            HStack{
                                Text("No me gusta")
                                Image(systemName: "hand.thumbsdown.fill")
                            }
                            
                        }).disabled(isDesactiveButtonLike)
                        Spacer().frame(width: 50 )
                        Button(action: {
                            isDesactiveButtonLike.toggle()
                            breedViewModel.saveLikeBreed(like: true, indexBreed: indexSeleted, image: self.image)
                        }, label: {
                            HStack{
                                Text("Me gusta")
                                Image(systemName: "hand.thumbsup.fill")
                            }
                            
                        }).disabled(isDesactiveButtonLike)
                    }
            }.navigationTitle("Votacion")
           
        }
        
        
    }
}

struct ImageCatView_Previews: PreviewProvider {
    static var previews: some View {
        VoteBreedView(imageLoader: ImageLoaderMV(), breedViewModel: BreedViewModel())
    }
}
