//
//  BreedModel.swift
//  catapp
//
//  Created by eddier caicedo on 26/08/21.
//

import SwiftUI
struct BreedModel: Decodable, Identifiable {
    var id: String
    var name : String
    var description : String
    var origin : String
}
