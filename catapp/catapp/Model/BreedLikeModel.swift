//
//  CatLikeModel.swift
//  catapp
//
//  Created by eddier caicedo on 28/08/21.
//

import SwiftUI

struct BreedLikeModel: Codable, Identifiable, Hashable {
    var id : UUID
    var fecha : String
    var like : Bool
    var image: Data
    var name : String
    var description: String
    var origin : String
}
