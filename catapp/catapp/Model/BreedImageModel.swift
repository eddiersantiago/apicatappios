//
//  CatModel.swift
//  catapp
//
//  Created by eddier caicedo on 28/08/21.
//

import SwiftUI

struct BreedImageModel: Decodable, Identifiable, Hashable {
    var id: String
    var url : String
}
