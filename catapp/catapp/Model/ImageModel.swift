//
//  ImageModel.swift
//  catapp
//
//  Created by eddier caicedo on 29/08/21.
//

import SwiftUI

struct ImageModel:  Decodable {
   var id: String
    var width: Int
    var height : Int
    var url: String

}
